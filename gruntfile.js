module.exports = function(grunt) {
	require('jit-grunt')(grunt);
	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		php : {
			watch : {
				options: {
//	                keepalive: true,
//	                open: true,
	                bin: 'C:/xampp/php/php.exe',
	                base: 'src'
	            }
			}
		},
		watch: {
			all : {
				files : ["src/**/*"],
				options : {
					livereload: true
				}
			}
		},
		cssmin: {
			release: {
				expand: true,
				cwd: 'release/css/',
				src: ['*.css', '!*.min.css'],
				dest: 'release/css/',
				ext: '.css'
			},
		},
		clean: {
			release: ["./release"]
		},
		copy : {
			release : {
				expand: true,
				cwd: './src',
				src : ["./**/*"],
				dest: "./release"
			},
		}
	});
	
	grunt.registerTask("default", ["php:watch", "watch"]);
	grunt.registerTask("build", ["clean:release", "copy:release", "cssmin:release"]);
};