
    <div class="row">
        <input placeholder="Name" name="email" type="text" class="full"/>
    </div>
    <div class="row">
        <input placeholder="Vorname" name="pw" type="text" class="full"/>
    </div>
    <div class="row">
        <input placeholder="PLZ" name="plz" type="text" class="full" id="plz"/>
    </div>
	<div class="row">
        <span id="cityName"></span>
    </div>
    <div class="row">
        <input placeholder="Geburtsdatum" name="birth" type="date" class="full"/>
    </div>
	<div class="row full">
	<select class="full">
		<option>Ernährungsweise...</option>
	    <option>mit Fleisch</option>
	    <option>vegetarisch</option>
	    <option>vegan</option>
	</select>
</div>
    <div class="row">
        <a href="index.php?t=bereiche" class=" pull-right">Weiter zum T&auml;tigkeitsfeld</a>
    </div>
	<script>
	$(function() {
		// OnKeyDown Function


		$("#plz").keyup(function() {
        
			var zip_in = $(this);
			var zip_box = $('#zipbox');
			
			if (zip_in.val().length<5)
			{
				zip_box.removeClass('error success');
			}
			else if ( zip_in.val().length>5)
			{
				zip_box.addClass('error').removeClass('success');
			}
			else if ((zip_in.val().length == 5) ) 
			{
				
				// Make HTTP Request
				$.ajax({
					url: "http://api.zippopotam.us/de/" + zip_in.val(),
					cache: false,
					dataType: "json",
					type: "GET",
				  success: function(result, success) {
						// Enable the Input for City or State
						$("#cityName").html("");
						$('#city').removeAttr('disabled');
						$('#state').removeAttr('disabled');

						// German Post Code Records Officially Map to only 1 Primary Location
						places = result['places'][0];
						$("#cityName").html(places['place name']);
						$("#state").val(places['state']);
						zip_box.addClass('success').removeClass('error');
					},
					error: function(result, success) {
						$("#cityName").html("");
					}
				});
			}
		});
	});
	</script>
