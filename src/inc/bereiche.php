<div class="row full">
	<select class="full" id="firstBereich">
		<option>Favorisierter Bereich...</option>
	    <option>Ordner</option>
	    <option>Handwerker</option>
	    <option>Seelsorge</option>
	    <option value="2">Fahrer</option>
	    <option></option>
	</select>
</div>

<div class="row full hidden driversclass">
	<!-- h4>Angaben für den Bereich Fahrer:</h4 -->
	<input placeholder="Führerscheinklasse" name="driversclass" type="text" class="full"/>
</div>

<div class="row full">
	<select class="full">
		<option>Alternativer Bereich...</option>
	    <option>Ordner</option>
	    <option>Handwerker</option>
	    <option>Seelsorge</option>
	    <option></option>
	    <option></option>
	</select>
</div>



<div class="row full">
	<label><input type="checkbox" />Ich möchte am Aufbau teilnehmen</label>
	<label><input type="checkbox" />Ich möchte am Abbau teilnehmen</label>
</div>
<div class="row full">
	<select class="full">
		<option>Bereich für Auf- und Abbau...</option>
	    <option>Ordner</option>
	    <option>Handwerker</option>
	    <option>Seelsorge</option>
	    <option></option>
	    <option></option>
	</select>
</div>

<div class="row full">
	<textarea placeholder="Erfahrungen" class="full" height="400px"></textarea>
</div>
<div class="row full">
	<textarea placeholder="Bemerkungen" class="full" height="400px"></textarea>
</div>
<div class="row full">
	<a href="index.php?t=anmelden" class="quater pull-left">Zur&uuml;ck</a>
	<a href="index.php?t=summary" class="quater pull-right">Weiter...</a>	
</div>
<script>
	$(function() {
		$("#firstBereich").change(function(){			
			if($(this).val() === "2"){
				$(".driversclass").removeClass("hidden");
			}
		});
	});
</script>
